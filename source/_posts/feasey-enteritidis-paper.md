---
title: Nick Feasey's Global Enteritidis Paper
date: 2019-03-18 11:59:50
tags:
- Enteritidis
- Phylogenetic
---

The paper title is *Distinct Salmonella Enteritidis lineages associated with enterocolitis in high-income settings and invasive disease in low-income settings*, Published on [Nature Genetics](https://www.nature.com/articles/ng.3644), 2016

## Overview

This study include 675 *S*. Enteritidis genomes from 45 countries, identified a global clade and 2 African clades. Find genomic degradation, new prophages and a MDR plasmid.

As a bioinformatician, I may even more interested in the methods rather than the conclusions.

## Methods

- Reads were aligned to P125109 using an "in house" pipeline: [SMALT](https://www.sanger.ac.uk/science/tools/smalt-0), a hashing-based aligner. Then the reads of each isolates were combined into a single BAM file with unmapped reads.
- SNPs were called by SAMtools mpileup program, produce a BCF file.
- Phylogenetic tree built with RAxML. Clade predicted using HierBAPS.

    >Phylogenetic modelling was based on the assumption of a single common ancestor; therefore, variable regions where horizontal genetic transfer occurs were excluded.

    This is interesting and confusing.

- 17 isolates were compared against 2,986 *S*. Enteritidis PHE genomes. Showed that the 2 African clades are geographically restricted to Africa. **This is how we prove a clade is only in some place**
- The BEAST tree used a relaxed lognormal clock model and constant population model. The way they exclude the other models is tricky, seems to be inspired by the BEAST book *Computational Evolution Book of BEAST2*
- BEAST was run in each clade.
- AMR gene was identified with a reads-based method. The threshold is 90% identity.
- Plasmid was extracted and sequenced separately.
- Some wet experiments were also included.

## Results

- The often mentioned phylogenetic tree:
    
    ![tree](https://media.nature.com/lw926/nature-assets/ng/journal/v48/n10/images/ng.3644-F1.jpg)

    4 major clades: global clade, central/eastern Africa clade,  West Africa clade, and an outlier cluster.

    It's interesting that one category of the antibiogram is "Extended-spectrum beta-lactamase producing"

- The 2 African clades are characterized by invasiveness and multiple drug resistance. The MRCA of the African clades date to 1934 and 1933.
- Figure 2 showed the differences of prophage and plasmids among each clades
- They also looked into pseudogenes or hypothetically disrupted genes, nonsynonymous SNPs.
- They **didn't** identify the key genes that responsible for the invasiveness. 


## Terminology

- multilocus variable number tandem repeat (MLVA) type
- extended-spectrum β-lactamase (ESBL)
- fimbrial operon
- hypothetically disrupted genes (HDGs)

## Words

- resembled
- paraphyletic
- cryptic
- plasticity
- intriguing
  
## Ideas

We need a phylogenetic level that between MLST and cgMLST, which can divide a serovar such as Enteritidis, or a very diversity sequence type such as ST19 into clusters, for example 10 clusters.

Evolution and phylogenetic of plasmids?
---
title: Grape tree
date: 2019-04-01 15:21:41
tags:
- Phylogenetic
---

This is the Grape tree paper, the paper is available at [Genome research](https://genome.cshlp.org/content/28/9/1395), the software of Grape tree is available at [Github](https://github.com/achtman-lab/GrapeTree), a [live demo](https://achtman-lab.github.io/GrapeTree/MSTree_holder.html) is available online. And GrapeTree is also a part of `Enterobase`, the document is included in [Enterobase doc](https://enterobase.readthedocs.io/en/latest/grapetree/grapetree-about.html)

## Overview

GrapeTree is a phylogenetic visualisation tool accept variable inputs (alignments, trees, metadata), collected phylogenetic algorithms (MStree, RapidNJ, MLtree), provide visualisation for huge amount of isolates (up to 100,000).

## Terminology

- Minimum spanning tree
- phylogram / dendrogram
- [RapidNJ](http://birc.au.dk/Software/RapidNJ/): a neighbour-joining algorithm
- [FastME](http://www.atgc-montpellier.fr/fastme/): an improvement of NJ algorithm

## Key potins

- The visual component frontend is based on HTML/JS and servered by Flask.
- Calculating trees is in an independent module (CL), which is based on NumPy and C++. Can be run in command line， end with a standard Newick format tree.
- GrapeTree is also a part of Enterobase and BigsDB
- The metadata will be visualised by colors.


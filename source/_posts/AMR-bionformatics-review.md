---
title: AMR bioinformatics review
date: 2019-03-25 19:10:26
tags: 
- AMR
- Bioinformatics
---

the paper title is *Sequencing-based methods and resources to study antimicrobial resistance*, published on [Nature Reviews Genetics](https://www.nature.com/articles/s41576-019-0108-4) (2019)

## Key points:

- Table 1 included the major tools currently. Most of them haven't been tried by me before. Will Rowe is involved, I can ask him if I want to develop a tool in the future.

- Compared assembly-based and read-based approach
    The big reason of using read-based approach is that it's faster because it bypasses the de novo assembly. But given we already have the assembly, it seems I have no reason to use this.

- Table 2 summarized all the databases. To choose a proper database is even more important than which tool to choose.
- The deep learning based tool [`DeepARG`](https://bitbucket.org/gusphdproj/deeparg-ss) is very attractive.

## Terminology

- whole-metagenome sequencing (WMS)
- Functional metagenomic

## Summary

This paper is a good review of the bioinformatic tools, databases, algorithms involved in the AMR. The approach I'm using now: `Abricate` + `Resfinder` seems good so far, according the this review and my experience. However some of the gene name in `Resfinder` needs to be corrected.

A *Salmonella* or enterobacterium specific database would be improvement.

To use machine learning or machine learning based tool seems attractive. I've read a paper use machine learning to predict AMR, gives higher accurate than the mainstream databases, especially on those genes that one gene shows small affect to AMR, while a combination of multiple genes shows obvious AMR phenotype. 

To develop machine approach, a good training data is necessary. It may from experiment, or from the exist databases. 